package com.eggmarket.restapi.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.persistence.EntityNotFoundException;

import com.eggmarket.restapi.controller.constants.EggsConstants;
import com.eggmarket.restapi.controller.constants.MapConstants;
import com.eggmarket.restapi.factory.EggFactory;
import com.eggmarket.restapi.model.Egg;
import com.eggmarket.restapi.model.enums.EggType;
import com.eggmarket.restapi.repository.EggRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EggService extends EggsConstants {

    private EggRepository eggRepository;

    private EggFactory eggFactory;

    public List<Egg> getAvailableEggs() {
        LOGGER.debug("GetAvailableEggs - obteniendo huevos  " + CLASS_NAME);
        return eggRepository.findAll();
    }

    public Egg getEggById(Long id) throws EntityNotFoundException {
        LOGGER.debug("GetEggById  - obteniendo huevo + ID:   " + CLASS_NAME);
        return eggRepository.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    public Egg createEgg(char eggType) {
        LOGGER.debug("  - eggtype : " + eggType + CLASS_NAME);
        Egg egg = eggFactory.getEgg(eggType);
        LOGGER.debug(" - eggGuardado : " + egg.toString() + CLASS_NAME);
        return egg;
    }

    public Map<String, Object> modifyEggById(Long eggId, char eggType) {
        LOGGER.debug(" - ID  : " + eggId + "Type: " + eggType + CLASS_NAME);
        Map<String, Object> responseService = new HashMap<>();
        try {
            if (eggRepository.findById(eggId).isPresent()) {
                LOGGER.debug("Existe huevo con ID. Modificando." + CLASS_NAME);
                // jpa hibernate no actualiza invocando todo en la misma sentencia, si no que
                // debo obtener la referencia, actualizar, y ghuardarla.
                Egg eggToModify = eggRepository.findById(eggId).get();
                LOGGER.debug("Setting eggType." + CLASS_NAME);
                eggToModify.setEggType(EggType.of(eggType));
                LOGGER.debug("Guardando eggType." + CLASS_NAME);
                Egg modified = eggRepository.save(eggToModify);
                LOGGER.debug("Eggtype guardado." + CLASS_NAME);
                responseService.put(MapConstants.STATUS_KEY.getValue(), MapConstants.STATUS_OK.getValue());
                responseService.put(MapConstants.INFO_KEY.getValue(), modified);
            } else {
                LOGGER.error("modifyEggById  - ID  : " + eggId + " no encontrado" + CLASS_NAME);
                responseService.put(MapConstants.STATUS_KEY.getValue(), MapConstants.STATUS_ERROR.getValue());
                responseService.put(MapConstants.INFO_KEY.getValue(), "Huevo a modificar no encontrado");
            }

        } catch (EntityNotFoundException enfE) {
            LOGGER.error("modifyEggById  - ID  : " + eggId + " no encontrado " + CLASS_NAME);
            responseService.put(MapConstants.STATUS_KEY.getValue(), MapConstants.STATUS_ERROR.getValue());
            responseService.put(MapConstants.INFO_KEY.getValue(), "Huevo a modificar no encontrado");
        } catch (IllegalArgumentException iae) {
            // mal charegg
            LOGGER.error("modifyEggById  - Parametro eggType incorrecto:" + eggType + CLASS_NAME);
            responseService.put(MapConstants.STATUS_KEY.getValue(), MapConstants.STATUS_ERROR.getValue());
            responseService.put(MapConstants.INFO_KEY.getValue(), "Egg type incorrecto: " + eggType);

        }
        return responseService;

    }

    public Map<String, Object> sellEgg(Long eggId, Long idBuyer) {
        Map<String, Object> responseService = new HashMap<>();
        LOGGER.debug("Intentando vender huevo" + CLASS_NAME);

        try {
            Egg eggToSell = eggRepository.findById(eggId).get();
            LOGGER.debug("Existe huevo con ID. Modificando." + CLASS_NAME);

            eggToSell.setIdBuyer(idBuyer);
            eggToSell.setVendido(true);
            LOGGER.debug("Setenando datos vendido." + CLASS_NAME);
            Egg modified = eggRepository.save(eggToSell);
            LOGGER.debug("Huevo vendido. Generando respuesta" + CLASS_NAME);
            responseService.put(MapConstants.STATUS_KEY.getValue(), MapConstants.STATUS_OK.getValue());
            responseService.put(MapConstants.INFO_KEY.getValue(), modified);

        } catch (EntityNotFoundException | NoSuchElementException enfE) {
            LOGGER.error("sellEgg  - ID  : " + eggId + " no encontrado " + CLASS_NAME);
            responseService.put(MapConstants.STATUS_KEY.getValue(), MapConstants.STATUS_ERROR.getValue());
            responseService.put(MapConstants.INFO_KEY.getValue(), "Huevo no encontrado");

        } catch (IllegalArgumentException iae) {
            LOGGER.error("sellEgg  - Parametros incorrectos: Vendedor:" + idBuyer + CLASS_NAME);
            responseService.put(MapConstants.STATUS_KEY.getValue(), MapConstants.STATUS_ERROR.getValue());
            responseService.put(MapConstants.INFO_KEY.getValue(), "Vendedor incorrecto: " + idBuyer);
        }
        return responseService;
    }

    public Map<String, String> deleteById(Long eggId) {
        Map<String, String> responseService = new HashMap<>();
        try {
            LOGGER.debug("Borrando por id." + eggId + CLASS_NAME);
            eggRepository.deleteById(eggId);
            LOGGER.debug("Borrado por id." + eggId + CLASS_NAME);
            responseService.put(MapConstants.STATUS_KEY.getValue(), MapConstants.STATUS_OK.getValue());
        } catch (IllegalArgumentException iaE) {
            LOGGER.error("Error borrando por ID ." +eggId + CLASS_NAME);
            responseService.put(MapConstants.STATUS_KEY.getValue(), MapConstants.STATUS_ERROR.getValue());

        }
        return responseService;

    }

    @Autowired
    public EggService(EggRepository eggRepository, EggFactory eggFactory) {
        this.eggRepository = eggRepository;
        this.eggFactory = eggFactory;

    }

    private static final Logger LOGGER = LogManager.getLogger(EggService.class);

    private static final String CLASS_NAME = "- - - " + EggService.class.getName();

}
