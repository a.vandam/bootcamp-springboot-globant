package com.eggmarket.restapi.repository;

import com.eggmarket.restapi.model.Egg;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EggRepository extends JpaRepository<Egg, Long> {

}
