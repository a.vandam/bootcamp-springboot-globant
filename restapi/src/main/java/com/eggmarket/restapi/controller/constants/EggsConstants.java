package com.eggmarket.restapi.controller.constants;

public abstract class EggsConstants {
    // TODO: pasar a archivo Json.
    protected static final String EGGS_GET_ALL_AVAILABLE = "/available";
    protected static final String EGGS_EGGID = "eggId";
    protected static final String EGGS_GET_BY_ID = "/{" + EGGS_EGGID + "}";
    protected static final String EGGS_MANAGEMENT = "/management";
    protected static final String EGGS_MANAGEMENT_BY_ID = EGGS_MANAGEMENT + EGGS_GET_BY_ID;
    protected static final String EGGS_SELL_BY_ID = EGGS_MANAGEMENT + "/sell" + EGGS_GET_BY_ID;

    protected static final String EGGS_MODIFY_OK_MSG = "Modificado huevo con exito. ID: ";
    protected static final String EGGS_MODIFY_ERROR_MSG = "Error en el Modificado huevo. Revise response. Huevo no encontrado. ID y eggType: ";
    protected static final String EGGS_GET_ALL_OK_MSG = "Huevos disponibles obtenidos.";
    protected static final String EGGS_GET_ALL_ERROR_MSG = "Huevos disponibles no obtenidos.";
    protected static final String EGGS_GET_BY_ID_OK_MSG = "Huevo encontrado.";
    protected static final String EGGS_GET_BY_ID_ERROR_MSG = "Huevo no encontrado.";
    protected static final String EGG_DELETE_OK_MSG = "Borrado de  huevo con exito.";
    protected static final String EGG_DELETE_ERROR_MSG = "Borrado de huevo fallo.";
    protected static final String EGG_SELL_OK_MSG = "Request de venta de huevo OK";
    protected static final String EGG_SELL_ERROR_MSG = "Venta de huevo fallo";
    protected static final String EGGS_CREATE_OK_MSG = "Creacion de huevo OK";
    protected static final String EGGS_CREATE_ERROR_MSG = "Creacion de huevo fallo";

    protected static final String EGGS_KEY_ID = "id";
    protected static final String EGGS_KEY_EGGTYPE = "eggType";
    protected static final String EGGS_KEY_IDBUYER = "idBuyer";

    protected EggsConstants() {

    }

}
