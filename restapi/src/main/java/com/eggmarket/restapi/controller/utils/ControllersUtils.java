package com.eggmarket.restapi.controller.utils;

import java.util.HashMap;
import java.util.Map;

import com.eggmarket.restapi.controller.constants.MapConstants;

import org.springframework.stereotype.Component;

@Component
public class ControllersUtils {
    public Map<String, Object> mapOKResponse(Object object, String message) {
        Map<String, Object> response = new HashMap<>();
        response.put(MapConstants.STATUS_KEY.getValue(), MapConstants.STATUS_OK.getValue());
        response.put(MapConstants.INFO_KEY.getValue(), message);
        response.put(MapConstants.RESPONSE_KEY.getValue(), object);
        return response;
    }

    public Map<String, Object> mapErrorResponse(Object object) {
        Map<String, Object> response = new HashMap<>();
        response.put(MapConstants.STATUS_KEY.getValue(), MapConstants.STATUS_ERROR.getValue());
        response.put(MapConstants.RESPONSE_KEY.getValue(), object);
        return response;
    }

    public Map<String, Object> mapErrorStringResponse(String string) {
        Map<String, Object> response = new HashMap<>();
        response.put(MapConstants.STATUS_KEY.getValue(), MapConstants.STATUS_ERROR.getValue());
        response.put(MapConstants.RESPONSE_KEY.getValue(), string);
        return response;
    }
}
