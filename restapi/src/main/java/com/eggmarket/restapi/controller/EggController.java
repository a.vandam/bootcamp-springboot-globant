package com.eggmarket.restapi.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityNotFoundException;

import com.eggmarket.restapi.controller.constants.EggsConstants;
import com.eggmarket.restapi.controller.utils.ControllersUtils;
import com.eggmarket.restapi.model.Egg;
import com.eggmarket.restapi.service.EggService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "eggs", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class EggController extends EggsConstants {

    private ControllersUtils utils;

    private EggService eggService;

    private static final Logger LOGGER = LogManager.getLogger(EggController.class);

    private static final String CLASS_NAME = "- - - " + EggController.class.getName();

    @GetMapping(path = EGGS_GET_ALL_AVAILABLE)
    public ResponseEntity<Map<String, Object>> getAvailableEggs() {
        try {
            LOGGER.info("Obteniendo todos los huevos." + CLASS_NAME);
            List<Egg> eggsList = eggService.getAvailableEggs();
            LOGGER.debug("Huevos obtenidos. Genberando response" + String.valueOf(eggsList) + CLASS_NAME);
            return new ResponseEntity<>(utils.mapOKResponse(eggsList, EGGS_GET_ALL_OK_MSG), HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Huevos no obtenidos.Error " + CLASS_NAME + e.getMessage());
            return new ResponseEntity<>(utils.mapErrorResponse(EGGS_GET_ALL_ERROR_MSG), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = EGGS_GET_BY_ID)
    public ResponseEntity<Map<String, Object>> getEgg(@PathVariable(value = EGGS_EGGID) Long eggId) {
        try {
            LOGGER.info("Obteniendo huevo de ID" + eggId + CLASS_NAME);
            Egg egg = eggService.getEggById(eggId);
            LOGGER.debug("Huevo obtenido de ID" + eggId + egg.toString() + CLASS_NAME);
            return new ResponseEntity<>(utils.mapOKResponse(egg, EGGS_GET_BY_ID_OK_MSG), HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Huevo no obtenidos.Error " + CLASS_NAME + e.getMessage());
            return new ResponseEntity<>(utils.mapErrorResponse(EGGS_GET_BY_ID_ERROR_MSG), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = EGGS_MANAGEMENT_BY_ID)
    public ResponseEntity<Map<String, Object>> modifyEgg(@PathVariable(value = EGGS_EGGID) Long eggId,
            @RequestBody Map<String, String> request) {
        char eggType = ' ';
        Map<String, Object> response = new HashMap<>();
        try {
            eggType = (request.get(EGGS_KEY_EGGTYPE).charAt(0));
            LOGGER.info("Obteniendo huevo de ID" + eggId + "tipo " + eggType + CLASS_NAME);
            response = eggService.modifyEggById(eggId, eggType);
            // Egg savedEgg = eggService.modifyEggById(eggId, eggType);
            LOGGER.debug("Huevo modificado. Generando response" + response.toString() + CLASS_NAME);
            return new ResponseEntity<>(utils.mapOKResponse(response, EGGS_MODIFY_OK_MSG), HttpStatus.OK);
        } catch (NullPointerException e) {
            LOGGER.error("Huevos no encontrado. Error " + CLASS_NAME + e.getMessage());
            return new ResponseEntity<>(
                    utils.mapErrorResponse(EGGS_MODIFY_ERROR_MSG + eggId + " - eggType:" + eggType + "."),
                    HttpStatus.BAD_REQUEST);
        }

    }

    @PostMapping(value = EGGS_SELL_BY_ID)
    public ResponseEntity<Map<String, Object>> sellEgg(@PathVariable(value = EGGS_EGGID) Long eggId,
            @RequestBody Map<String, String> request) {
        Map<String, Object> response = new HashMap<>();

        try {
            LOGGER.info("Vendiendo huevos.  " + CLASS_NAME);
            Long idBuyer = Long.parseLong(request.get(EGGS_KEY_IDBUYER));
            LOGGER.info("ID " + idBuyer + CLASS_NAME);
            response = eggService.sellEgg(eggId, idBuyer);
            LOGGER.debug("Huevos obtenidos. Genberando response" + response.toString() + CLASS_NAME);
            return new ResponseEntity<>(utils.mapOKResponse(response, EGG_SELL_OK_MSG), HttpStatus.OK);
        } catch (NumberFormatException nfe) {
            LOGGER.error("Huevos no vendido .Error " + CLASS_NAME + nfe.getMessage());

            return new ResponseEntity<>(utils.mapErrorResponse(EGG_SELL_ERROR_MSG), HttpStatus.BAD_REQUEST);
        }

    }

    // idempotent, creation.
    @PutMapping(value = EGGS_MANAGEMENT)
    public ResponseEntity<Map<String, Object>> createEgg(@RequestBody Map<String, String> request) {
        try {
            char eggType = request.get(EGGS_KEY_EGGTYPE).charAt(0);
            Egg egg = eggService.createEgg(eggType);
            return new ResponseEntity<>(utils.mapOKResponse(egg, EGGS_CREATE_OK_MSG), HttpStatus.OK);
        } catch (NullPointerException | EntityNotFoundException nfe) {
            LOGGER.error("Huevos no creado.Error " + CLASS_NAME + nfe.getMessage());
            return new ResponseEntity<>(utils.mapErrorResponse(EGGS_CREATE_ERROR_MSG), HttpStatus.BAD_REQUEST);
        }
    }

    // idempotent, creation.
    @DeleteMapping(value = EGGS_MANAGEMENT_BY_ID)
    // @ResponseBody
    public ResponseEntity<Map<String, Object>> deleteEgg(@PathVariable(value = EGGS_EGGID) Long eggId) {
        try {
            Map<String, String> serviceResponse = eggService.deleteById(eggId);
            LOGGER.debug("Huevo borrado. " + "Response: " + serviceResponse.get("status") + CLASS_NAME);
            return new ResponseEntity<>(utils.mapOKResponse(serviceResponse, EGG_SELL_OK_MSG), HttpStatus.OK);
        } catch (NumberFormatException | EntityNotFoundException nfe) {
            LOGGER.error("Huevos no borrado.Error " + CLASS_NAME + nfe.getMessage());
            return new ResponseEntity<>(utils.mapErrorResponse(EGG_SELL_ERROR_MSG), HttpStatus.BAD_REQUEST);
        }
    }

    // Autowired controller for dependency inj
    @Autowired
    public EggController(EggService eggService, ControllersUtils utils) {
        super();
        this.eggService = eggService;
        this.utils = utils;

    }
}
