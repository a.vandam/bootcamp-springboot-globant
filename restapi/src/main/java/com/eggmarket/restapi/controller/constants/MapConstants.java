package com.eggmarket.restapi.controller.constants;

public enum MapConstants {

    STATUS_KEY("status"), STATUS_OK("ok"), STATUS_ERROR("error"), RESPONSE_KEY("response"), RESPONSE_OK(""),
    INFO_KEY("info");

    private final String enumValue;

    // Cada Key es key_MAP. Cada valor, KEY_valor.

    public String getValue() {
        return this.enumValue;
    }
    // Empty constructor for final class.

    private MapConstants(String enumValue) {
        this.enumValue = enumValue;

    }

}
