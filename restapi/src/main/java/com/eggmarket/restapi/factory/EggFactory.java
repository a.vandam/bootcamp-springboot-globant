package com.eggmarket.restapi.factory;

import com.eggmarket.restapi.model.Egg;
import com.eggmarket.restapi.model.enums.EggType;
import com.eggmarket.restapi.repository.EggRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EggFactory {
    private EggRepository eggRepository;

    public Egg getEgg(char eggChar) {
        if (eggChar == ' ') {
            return null;
        }
        if (Character.toUpperCase(eggChar) == EggType.RED.getEggChar()) {
            Egg eggToBeSaved = new Egg(EggType.RED);
            return eggRepository.save(eggToBeSaved);
        } else if (Character.toUpperCase(eggChar) == EggType.WHITE.getEggChar()) {
            Egg eggToBeSaved = new Egg(EggType.WHITE);
            return eggRepository.save(eggToBeSaved);
            // return eggRepository.save(new Egg(EggType.WHITE));
        }
        return null;
    }

    @Autowired
    private EggFactory(EggRepository eggRepository) {
        this.eggRepository = eggRepository;
    }

}
