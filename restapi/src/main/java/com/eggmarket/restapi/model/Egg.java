package com.eggmarket.restapi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.eggmarket.restapi.model.enums.EggType;
import com.sun.istack.NotNull;

import java.util.Objects;

@Entity
@Table(name = "eggs")
// @DynamicUpdate
public class Egg {
    @Id
    @Column(name = "egg_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // @Transient
    @Column(name = "egg_type_char", length = 1, nullable = true, unique = false)
    private char eggTypeChar;

    @Column(name = "vendido")
    private boolean vendido;

    @Column(name = "idbuyer")
    private Long idBuyer;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public char getEggTypeChar() {

        return eggTypeChar;
    }

    public EggType getEggType() {
        return EggType.of(eggTypeChar);
    }

    public void setEggType(EggType eggColor) {
        this.eggTypeChar = eggColor.getEggChar();
    }

    public boolean isVendido() {
        return vendido;
    }

    public void setVendido(boolean vendido) {
        this.vendido = vendido;
    }

    public Long getIdBuyer() {
        return idBuyer;
    }

    public void setIdBuyer(Long idBuyer) {
        this.idBuyer = idBuyer;
    }

    public Egg(EggType eggType) {
        this.eggTypeChar = Objects.requireNonNull(eggType.getEggChar(),"los huevos siempre tienen tipo");
        this.vendido = false;
    }

    public Egg(char eggTypeChar) {
        this.eggTypeChar = eggTypeChar;
        this.vendido = false;
    }

    @Override
    public String toString() {
        return "Huevo de id " + this.id + "con tipo " + this.eggTypeChar + "- vendido?" + this.vendido + " idBuyer "
                + this.idBuyer + " . ";
    }

    public Egg() {

    }
}
