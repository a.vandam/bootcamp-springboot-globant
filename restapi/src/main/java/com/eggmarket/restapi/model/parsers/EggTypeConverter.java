package com.eggmarket.restapi.model.parsers;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.stream.Stream;

import com.eggmarket.restapi.model.enums.EggType;

@Converter(autoApply = true)
public class EggTypeConverter implements AttributeConverter<EggType, Character> {

    @Override
    public Character convertToDatabaseColumn(EggType eggType) {
        if (eggType == null) {
            return null;
        }
        return eggType.getEggChar();
    }

    @Override
    public EggType convertToEntityAttribute(Character code) {
        if (code == null) {
            return null;
        }
        return Stream.of(EggType.values()).filter(eggType -> eggType.getEggChar() == (code.charValue())).findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

}
