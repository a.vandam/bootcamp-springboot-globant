package com.eggmarket.restapi.model.enums;

import java.util.stream.Stream;

public enum EggType {
    WHITE('W', "White", 1), RED('R', "Red", 2);

    private Character eggChar;
    private String eggTypeName;
    private int eggTypeInt;

    private EggType(char eggChar, String typeName, int eggTypeInt) {
        this.eggChar = eggChar;
        this.eggTypeInt = eggTypeInt;
        this.eggTypeName = typeName;
    }

    public static EggType of(char eggChar) throws IllegalArgumentException {
        return Stream.of(EggType.values()).filter(e -> e.getEggChar() == eggChar).findFirst()
                .orElseThrow(IllegalArgumentException::new);

    }

    public char getEggChar() {
        return eggChar;
    }

    public void setEggChar(char eggChar) {
        this.eggChar = eggChar;
    }

    public int getEggTypeInt() {
        return eggTypeInt;
    }

    public void setEggType(int eggTypeInt) {
        this.eggTypeInt = eggTypeInt;
    }

    public String getEggTypeName() {
        return eggTypeName;
    }

    public void setEggTypeName(String eggTypeName) {
        this.eggTypeName = eggTypeName;
    }

}
