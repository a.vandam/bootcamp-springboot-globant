DROP TABLE IF EXISTS eggs;

CREATE TABLE eggs (
  egg_id INT AUTO_INCREMENT  PRIMARY KEY,
  egg_type_char CHAR(1),
  vendido boolean,
  idbuyer INT
);
CREATE TABLE users (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  usuario VARCHAR(250) NOT NULL,
  nombre VARCHAR(250) NOT NULL,
  apellido VARCHAR(250) NOT NULL,
  rol INT NOT NULL
);
-- 1 nombre de usuario a la vez
ALTER TABLE users
ADD CONSTRAINT USUARIO_UNICO_C UNIQUE (id,usuario);

-- Creo usuarios para BD .
INSERT INTO users (usuario, nombre, apellido,rol) VALUES
  ('UsuarioPrueba1', 'Diego', 'Maradona',1),
  ('UsuarioPrueba2', 'Diego', 'Maradona',0),
  ('UsuarioPrueba3', 'Diego', 'Maradona',0);

-- INSERT INTO eggs (egg_color) VALUES
--   ('R'),
--   ('B'),
--   ('R');
  
