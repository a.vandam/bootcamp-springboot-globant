# BootCamp SpringBoot Globant

Ultimo ejercicio del Bootcamp Spring Boot de Globant, Marzo 2021. 
Hacer una api CRUD de Huevos, con dos colores, que permita venderlo.  

> Hecho con: 
> Java 11.
> Spring Boot 2.4.1.
> Spring Web 2.4.1.
> JPA Hibernate 
> JUnit + Mockito.
> Incluye Swagger2.

DISCLAIMER: Not maintained. Will be deleted in short time. 
